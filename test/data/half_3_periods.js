{"institution": {"buildings": [[{"capacity": 30}]],
                 "teachers": [{}],
                 "classes": [{"size": 30}, {"size": 30}, {"size": 29}],
                 "subjects": [{}],
                 "days": [[2]]},
 "max_dominant": 2,
 "activities": [{"subactivities": [{"teachers": [0], "classes": [0], "subjects": [0], "schedules": [{"day": 0, "hour": 0, "building": 0, "room": 0}]}], "durations": [0.5]},
                {"subactivities": [{"teachers": [0], "classes": [1], "subjects": [0], "schedules": [{"day": 0, "hour": 0, "building": 0, "room": 0}]}], "durations": [0.5]},
                {"subactivities": [{"teachers": [0], "classes": [2], "subjects": [0], "schedules": [{"day": 0, "hour": 1, "building": 0, "room": 0}]}], "durations": [0.5]}]
}
