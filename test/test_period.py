from baklava.fet import TimeTable
from json import loads
from lxml.etree import XML
from os.path import isfile


def _test(name):
    json = loads(open('test/data/{}.js'.format(name), 'r').read())
    xml = open('test/data/{}.fet'.format(name), 'r').read()
    timetable = TimeTable(json)
    assert xml == timetable.tofet()
    #
    tojson = timetable.tojson()
    assert json == tojson


def test_period():
    _test('period_1')


def test_two_periods():
    _test('period_2')


def test_break_periods():
    _test('period_3')


def test_two_days_periods():
    _test('period_4')


def test_two_days_periods_not_equal():
    _test('period_5')


def test_three_days_periods_not_equal():
    _test('period_6')
