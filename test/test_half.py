from baklava.fet import TimeTable
from json import loads
from lxml.etree import XML
from os.path import isfile


def _test(name):
    xml = open('test/data/{}.fet'.format(name), 'r').read()
    json = loads(open('test/data/{}.js'.format(name), 'r').read())
    jsonperiod = loads(open('test/data/{}_periods.js'.format(name), 'r').read())
    fetxml = XML(open('test/data/{}_teachers.xml'.format(name), 'r').read())
    timetable = TimeTable(json)
    assert xml == timetable.tofet()
    #
    tojson = timetable.tojson()
    assert json == tojson
    #
    timetable.fromfet(fetxml)
    tojson = timetable.tojson()
    assert jsonperiod == tojson


def test_half_one_duration():
    _test('half_1')

def test_half_two_durations():
    _test('half_2')

def test_half_three_durations():
    _test('half_3')

def test_half_two_durations_two_groups():
    _test('half_4')

def test_half_three_durations_two_groups():
    _test('half_5')

def test_half_four_durations_two_groups():
    #FIXME les 2 0.5 doivent ne pas etre les memes jours
    _test('half_6')

#several 0.5 not allowed
#test sur maxdominant !

#a faire :
# les salles preferees
# les breaks
