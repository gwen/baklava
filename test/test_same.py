from baklava.fet import TimeTable
from baklava.error import ParamError
from json import loads
from lxml.etree import XML
from py.test import raises
from os.path import isfile


def _test(name):
    xml = open('test/data/{}.fet'.format(name), 'r').read()
    json = loads(open('test/data/{}.js'.format(name), 'r').read())
    jsonperiod = loads(open('test/data/{}_periods.js'.format(name), 'r').read())
    fetxml = XML(open('test/data/{}_teachers.xml'.format(name), 'r').read())
    timetable = TimeTable(json)
    assert xml == timetable.tofet()
    #
    tojson = timetable.tojson()
    assert json == tojson
    #
    timetable.fromfet(fetxml)
    tojson = timetable.tojson()
    assert jsonperiod == tojson


def test_same_duration_no_force_nb_students():
    obj = {'institution': {'buildings': [[{'capacity': 30}]],
                           'teachers': [{}, {}],
                           'classes': [{'size': 30}],
                           'subjects': [{}],
                           'days': [[1]]},
           'max_dominant': 2,
           'activities': [{'subactivities': [{'teachers': [0], 'classes': [0], 'subjects': [0]},
                                             {'teachers': [1], 'classes': [0], 'subjects': [0]}],
                               'durations': [1, 1]}]
          }
    raises(ParamError, 'TimeTable(obj).tofet()')


def test_same_duration_one_force_nb_students():
    obj = {'institution': {'buildings': [[{'capacity': 30}]],
                           'teachers': [{}, {}],
                           'classes': [{'size': 30}],
                           'subjects': [{}],
                           'days': [[1]]},
           'max_dominant': 2,
           'activities': [{'subactivities': [{'teachers': [0], 'classes': [0], 'subjects': [0]},
                                             {'teachers': [1], 'classes': [0], 'subjects': [0], 'force_nb_students': 10}],
                               'durations': [1, 1]}]
          }
    raises(ParamError, 'TimeTable(obj).tofet()')


def test_same_duration_one_force_nb_students2():
    obj = {'institution': {'buildings': [[{'capacity': 30}]],
                           'teachers': [{}, {}],
                           'classes': [{'size': 30}],
                           'subjects': [{}],
                           'days': [[1]]},
           'max_dominant': 2,
           'activities': [{'subactivities': [{'teachers': [0], 'classes': [0], 'subjects': [0], 'force_nb_students': 10},
                                             {'teachers': [1], 'classes': [0], 'subjects': [0]}],
                               'durations': [1, 1]}]
          }
    raises(ParamError, 'TimeTable(obj).tofet()')


def test_same_duration_force_nb_students():
    _test('same_1')


#def test_same_duration_force_nb_students_and_half():
#    fh = open('test/data/same_2.fet', 'r')
#    xml = fh.read()
#    fh.close()
#    obj = {'institution': {'buildings': [[{'capacity': 30}]],
#                           'teachers': [{}, {}],
#                           'classes': [{'size': 30}],
#                           'subjects': [{}],
#                           'days': [[1]]},
#           'max_dominant': 2,
#           'activities': [{'subactivities': [{'teachers': [0], 'classes': [0], 'subjects': [0], 'force_nb_students': 10},
#                                             {'teachers': [1], 'classes': [0], 'subjects': [0], 'force_nb_students': 10}],
#                           'durations': [1, 1, 0.5]}]
#          }
#    assert xml == TimeTable(obj).tofet()
#FIXME student different
#FIXME duration de longueur 1
