from baklava.fet import TimeTable
from json import loads
from lxml.etree import XML
from os.path import isfile


def _test(name):
    json = loads(open('test/data/{}.js'.format(name), 'r').read())
    #xml = open('test/data/{}.fet'.format(name), 'r').read()
    if isfile('test/data/{}_periods.js'.format(name)):
        jsonperiod = loads(open('test/data/{}_periods.js'.format(name), 'r').read())
    else:
        jsonperiod = None
    if isfile('test/data/{}_teachers.xml'.format(name)):
        fetxml = XML(open('test/data/{}_teachers.xml'.format(name), 'r').read())
    else:
        fetxml = None
    timetable = TimeTable(json)
#    assert xml == timetable.tofet()
#    #
#    tojson = timetable.tojson()
#    print tojson
#    assert json == tojson
#    #
#    if fetxml is None:
#        print "FIXME"
#    else:
#        timetable.fromfet(fetxml)
#        tojson = timetable.tojson()
#        assert jsonperiod == tojson

def test_big():
    _test('big')

