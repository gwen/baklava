from baklava.fet import TimeTable
from json import loads
from lxml.etree import XML
from os.path import isfile


def _test(name):
    xml = open('test/data/{}.fet'.format(name), 'r').read()
    json = loads(open('test/data/{}.js'.format(name), 'r').read())
    jsonperiod = loads(open('test/data/{}_periods.js'.format(name), 'r').read())
    fetxml = XML(open('test/data/{}_teachers.xml'.format(name), 'r').read())
    timetable = TimeTable(json)
    assert xml == timetable.tofet()
    #
    tojson = timetable.tojson()
    assert json == tojson
    #
    timetable.fromfet(fetxml)
    tojson = timetable.tojson()
    assert jsonperiod == tojson


def test_simple_one_duration():
    _test('simple_1')


def test_simple_one_duration_two():
    _test('simple_2')


def test_simple_two_durations():
    _test('simple_3')
