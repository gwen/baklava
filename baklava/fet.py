from lxml.etree import Element, SubElement, tostring
from .error import ParamError
from json import loads, dumps


class _FET(object):
    def __init__(self, fet):
        self.fet = fet

    def add(self, key, value=None):
        xml = SubElement(self.fet, key)
        if value is not None:
            if isinstance(value, int):
                value = str(value)
            xml.text = value
        return _FET(xml)


class FET(_FET):
    def __init__(self):
        self.fet = Element('fet', version='5.27.9')
        self.add('Institution_Name', 'institution')
        self.add('Comments')

    def add_space_constrainte(self):
        self.space_constrainte = self.add('Space_Constraints_List')
        subxml = self.space_constrainte.add('ConstraintBasicCompulsorySpace')
        subxml.add('Weight_Percentage', '100')
        subxml.add('Active', 'true')
        subxml.add('Comments')
        #self.space_constrainte.add('ConstraintTeacherHomeRooms')

    def add_time_constrainte(self):
        self.time_constrainte = self.add('Time_Constraints_List')
        subxml = self.time_constrainte.add('ConstraintBasicCompulsoryTime')
        subxml.add('Weight_Percentage', '100')
        subxml.add('Active', 'true')
        subxml.add('Comments')

    def tostring(self):
        return tostring(self.fet, pretty_print=True, xml_declaration=True, encoding='UTF-8')


class Room(object):
    def __init__(self, idx, room, building):
        self.idx = idx
        self.building = building
        self.capacity = room['capacity']
        self.reserved = False

    def getname(self):
        return str(self.building.idx) + '_' + str(self.idx)

    def tofet(self, fet):
        subxml = fet.add('Room')
        subxml.add('Name', self.getname())
        subxml.add('Building', self.building.idx)
        subxml.add('Capacity', self.capacity)

    def tojson(self):
        return {'capacity': self.capacity}


class Building(object):
    def __init__(self, idx, building):
        self.idx = idx
        self.rooms = []
        for idx, room in enumerate(building):
            self.rooms.append(Room(idx, room, self))

    def tofet(self, fet, xml_building, xml_room):
        xml_building.add('Building').add('Name', self.idx)
        available_rooms = []
        for room in self.rooms:
            room.tofet(xml_room)
            if not room.reserved:
                available_rooms.append(room.getname())
        len_available_rooms = len(available_rooms)
        if len_available_rooms == 1:
            constraint_room = fet.space_constrainte.add('ConstraintActivityTagPreferredRoom')
        else:
            constraint_room = fet.space_constrainte.add('ConstraintActivityTagPreferredRooms')
        constraint_room.add('Weight_Percentage', '100')
        constraint_room.add('Activity_Tag', 'all_rooms')
        if len_available_rooms == 1:
            constraint_room.add('Room', available_rooms[0])
        else:
            constraint_room.add('Number_of_Preferred_Rooms', len_available_rooms)
        constraint_room.add('Active', 'true')
        constraint_room.add('Comments')
        if len_available_rooms != 1:
            for room in available_rooms:
                constraint_room.add('Preferred_Room', room)

    def tojson(self):
        ret = []
        for room in self.rooms:
            ret.append(room.tojson())
        return ret


class Buildings(object):
    def __init__(self, buildings):
        self.buildings = []
        for idx, building in enumerate(buildings):
            self.buildings.append(Building(idx, building))

    def tofet(self, fet):
        #building
        xml_building = fet.add('Buildings_List')
        xml_room = fet.add('Rooms_List')
        for building in self.buildings:
            building.tofet(fet, xml_building, xml_room)

    def tojson(self):
        ret = []
        for building in self.buildings:
            ret.append(building.tojson())
        return ret

    def find(self, idx):
        building, room = idx.split('_')
        return self.buildings[int(building)].rooms[int(room)]

class Teacher(object):
    def __init__(self, idx, teacher):
        self.idx = idx
        #FIXME : hum pas terrible, on devrait prendre les infos
        self.teacher = teacher
        self.half_durations = []

    def tofet(self, fet):
        fet.add('Teacher').add('Name', self.idx)

    def tojson(self):
        return self.teacher


class Teachers(object):
    def __init__(self, teachers):
        self.teachers = []
        for idx, teacher in enumerate(teachers):
            self.teachers.append(Teacher(idx, teacher))

    def tofet(self, fet): 
        #teacher
        xml = fet.add('Teachers_List')
        for teacher in self.teachers:
            teacher.tofet(xml)

    def tojson(self):
        ret = []
        for teacher in self.teachers:
            ret.append(teacher.tojson())
        return ret


class Class(object):
    def __init__(self, idx, klass):
        self.idx = idx
        self.size = klass['size']
        self.subgroup = -1
        self.xml = None

    def tofet(self, fet):
        self.xml = fet.add('Group')
        self.xml.add('Name', self.idx)
        self.xml.add('Number_of_Students', self.size)

    def add_subgroup(self):
        self.subgroup += 1
        name = str(self.idx) + '_' + str(self.subgroup)
        self.xml.add('Subgroup').add('Name', name)
        return name

    def tojson(self):
        return {'size': self.size}


class Classes(object):
    def __init__(self, classes):
        self.classes = []
        for idx, klass in enumerate(classes):
            self.classes.append(Class(idx, klass))

    def tofet(self, fet):
        xml = fet.add('Students_List').add('Year')
        #FIXME useful?
        xml.add('Name', 'G')
        xml.add('Number_of_Students', '0')
        for klass in self.classes:
            klass.tofet(xml)

    def tojson(self):
        ret = []
        for klass in self.classes:
            ret.append(klass.tojson())
        return ret


class Subject(object):
    def __init__(self, idx, subject):
        self.idx = idx
        self.dominant = subject.get('dominant', False)

    def tofet(self, fet):
        fet.add('Subject').add('Name', self.idx)

    def tojson(self):
        ret = {}
        if self.dominant is not False:
            ret['dominant'] = self.dominant
        return ret


class Subjects(object):
    def __init__(self, subjects):
        self.subjects = []
        for idx, subject in enumerate(subjects):
            self.subjects.append(Subject(idx, subject))

    def tofet(self, fet):
        xml = fet.add('Subjects_List')
        for subject in self.subjects:
            subject.tofet(xml)

    def tojson(self):
        ret = []
        for subject in self.subjects:
            ret.append(subject.tojson())
        return ret


class Days(object):
    def __init__(self, json, max_dominant):
        self.days = json
        self.max_dominant = max_dominant
        self.periods = []
        for day in self.days:
            for idx, period in enumerate(day):
                if idx < len(self.periods):
                    self.periods[idx] = max(self.periods[idx], period)
                else:
                    self.periods.append(period)
        len_periods = len(self.periods)
        for idx, day in enumerate(self.days):
            len_day = len(day)
            if len_day != len_periods:
                for period in xrange(len_day, len_periods):
                    self.days[idx].append(0)

    def tojson(self):
        return self.days

    def tofet(self, fet):
        #days
        self.nb_days = len(self.days)
        xml = fet.add('Days_List')
        xml.add('Number_of_Days', self.nb_days)
        for idx in xrange(self.nb_days):
            xml.add('Day').add('Name', idx)

        #hours
        # sum of all periods + breaks
        # for example [2, 2, 2, 2] means 2+2+2+2 periods + 3 breaks (4-1)
        # so 11
        periods = sum(self.periods) + len(self.periods) - 1
        xml = fet.add('Hours_List')
        xml.add('Number_of_Hours', periods)
        for idx in xrange(periods):
            xml.add('Hour').add('Name', str(idx) + ':00')
        #xml = self.fet.find('Time_Constraints_List')
        #for day_idx in xrange(self.institution['days']):
        #    subxml = SubElement(xml, 'ConstraintActivitiesOccupyMaxTimeSlotsFromSelection')
        #    SubElement(subxml, 'Weight_Percentage').text = '100'
        #    SubElement(subxml, 'Active').text = 'true'
        #    SubElement(subxml, 'Comments')
        #    selected = 0
        #    for period in self.institution['periods']:
        #        for idx in xrange(period):
        #            subsubxml = SubElement(subxml, 'Selected_Time_Slot')
        #            SubElement(subsubxml, 'Selected_Day').text = str(day_idx)
        #            SubElement(subsubxml, 'Selected_Hour').text = str(selected) + ':00'
        #            selected += 1
        #        selected += 1
        #    selected -= 2
        #    dominant_idx = 0
        #    # FIXME : Rien a voir !!! activity_id !!! pas subject !!!
        #    for idx, dominant in enumerate(self.institution['subjects']['all']):
        #        if dominant:
        #            dominant_idx += 1;
        #            SubElement(subxml, 'Activity_Id').text = str(idx)
        #    SubElement(subxml, 'Max_Number_of_Occupied_Time_Slots').text = str(self.institution['students']['max_dominant'])
        #    SubElement(subxml, 'Number_of_Activities').text = str(dominant_idx)
        #    SubElement(subxml, 'Number_of_Selected_Time_Slots').text = str(selected)

    def fetconstraints(self, xml, activities):
        breaktimes_len = 0
        subxml = None
        def get_subxml(subxml):
            if subxml is None:
                subxml = xml.add('ConstraintBreakTimes')
                subxml.add('Weight_Percentage', '100')
            return subxml
        def add_break(day_idx, breaktime):
            subsubxml = subxml.add('Break_Time')
            subsubxml.add('Day', day_idx)
            subsubxml.add('Hour', str(breaktime) + ':00')
            return breaktimes_len

        nb_break = len(self.periods) - 1
        for day_idx, periods in enumerate(self.days):
            break_period = 0
            for idx, period in enumerate(periods):
                # if period is smaller than other period
                for breaktime in xrange(period, self.periods[idx]):
                    subxml = get_subxml(subxml)
                    add_break(day_idx, breaktime + break_period)
                    breaktimes_len += 1
                break_period += self.periods[idx]
                if idx != nb_break:
                    subxml = get_subxml(subxml)
                    add_break(day_idx, break_period)
                    breaktimes_len += 1
                break_period += 1
        if subxml != None:
            subxml.add('Number_of_Break_Times', breaktimes_len)
            subxml.add('Active', 'true')
            subxml.add('Comments')
        # Not more than one gaps per week for children
        # Not more than two gaps per week for teachers
        subxml = xml.add('ConstraintStudentsMaxGapsPerWeek')
        subxml.add('Weight_Percentage', '100')
        subxml.add('Max_Gaps', '1')
        subxml.add('Active', 'true')
        subxml.add('Comments')
        #
        subxml = xml.add('ConstraintTeachersMaxGapsPerWeek')
        subxml.add('Weight_Percentage', '100')
        subxml.add('Max_Gaps', '2')
        subxml.add('Active', 'true')
        subxml.add('Comments')
        #Dominant
        for day_idx in xrange(len(self.days)):
            dominants_idx = []
            for activity in activities.activities:
                one_is_dominant = False
                for subactivity in activity.subactivities:
                    for subject in subactivity.subjects:
                        if subject.dominant is True:
                            one_is_dominant = True
                            break
                if one_is_dominant:
                    dominants_idx.append(activity.idx)
            if dominants_idx != []:
                subxml = xml.add('ConstraintActivitiesOccupyMaxTimeSlotsFromSelection')
                subxml.add('Weight_Percentage', '100')
                subxml.add('Active', 'true')
                subxml.add('Comments')
                selected = 0
                for period in self.periods:
                    for idx in xrange(period):
                        subsubxml = subxml.add('Selected_Time_Slot')
                        subsubxml.add('Selected_Day', day_idx)
                        subsubxml.add('Selected_Hour', str(selected) + ':00')
                        selected += 1
                    selected += 1
                selected -= 2
                for idx in dominants_idx:
                    subxml.add('Activity_Id', idx)
                subxml.add('Max_Number_of_Occupied_Time_Slots', self.max_dominant)
                subxml.add('Number_of_Activities', len(dominants_idx))
                subxml.add('Number_of_Selected_Time_Slots', selected)


class Duration(object):
    def __init__(self, idx, duration, subactivity):
        self.idx = idx
        self.duration = duration
        self.subactivity = subactivity
        self.schedules = []

    def add_classes(self, xml, add_subgroup):
        for klass in self.subactivity.classes:
            if add_subgroup:
                idx = klass.add_subgroup()
            else:
                idx = klass.idx
            xml.add('Students', idx)

    def add_activity_tag(self, xml):
        xml.add('Activity_Tag', 'all_rooms')
        xml.add('Activity_Tag', self.idx)

    def tofet(self, activity, tags_list, activity_list, time_constrainte_between, add_subgroup):
        tags_list.add('Activity_Tag').add('Name', self.idx)
        xml = activity_list.add('Activity')
        for teacher in self.subactivity.teachers:
            xml.add('Teacher', teacher.idx)
        #FIXME si le meme enseignant avec 2 cours differents ... pas gerer ici
        for subject in self.subactivity.subjects:
            xml.add('Subject', subject.idx)
        #FIXME pas tout le temps !
        self.add_activity_tag(xml)
        self.add_classes(xml, add_subgroup)
        if self.subactivity.force_nb_students != None:
            xml.add('Number_Of_Students', self.subactivity.force_nb_students)
        elif isinstance(self, HalfDuration) and self.second_activity is not None:
            xml.add('Number_Of_Students', self.nb_students)
        #FIXME est-ce correct dans le cas d'un half ? (puisqu'il y a 2 activites ...)
        time_duration = self.duration
        if time_duration == 0.5:
            time_duration = 1
        xml.add('Duration', time_duration)
        xml.add('Total_Duration', time_duration)
        xml.add('Id', self.idx)
        xml.add('Activity_Group_Id', self.idx)
        xml.add('Active', 'true')
        xml.add('Comments')
        if time_constrainte_between is not None:
            time_constrainte_between.add('Activity_Id', self.idx)


class HalfDuration(Duration):
    def __init__(self, idx, duration, activity):
        super(HalfDuration, self).__init__(idx, duration, activity)
        self.second_activity = None
        self.second_idx = None
        self.week = None
        self.nb_students = activity.nb_students

    def add_classes(self, xml, add_subgroup):
        super(HalfDuration, self).add_classes(xml, add_subgroup)
        if self.second_activity != None:
            for klass in self.second_activity.classes:
                if add_subgroup:
                    idx = klass.add_subgroup()
                else:
                    idx = klass.idx
                xml.add('Students', idx)

    def add_activity_tag(self, xml):
        super(HalfDuration, self).add_activity_tag(xml)
        if self.second_activity is not None:
            xml.add('Activity_Tag', self.second_idx)

    def tofet(self, activity, tags_list, activity_list, time_constrainte_between, add_subgroup):
        # half_duration already build for principal activity
        if self.second_activity == activity:
            tags_list.add('Activity_Tag').add('Name', self.second_idx)
            if time_constrainte_between is not None:
                time_constrainte_between.add('Activity_Id', self.idx)
        else:
            super(HalfDuration, self).tofet(activity, tags_list, activity_list, time_constrainte_between, add_subgroup)
        

class SubActivity(object):
    def __init__(self, idx, json, institution, durations, len_subactivities):
        self.idx = idx
        self.teachers = []
        self.classes = []
        self.subjects = []
        self.durations = []
        self.total_duration = 0
        self.force_nb_students = json.get('force_nb_students')
        self.nb_students = 0
        for teacher in json['teachers']:
            self.teachers.append(institution.teachers.teachers[teacher])
        for klass in json['classes']:
            class_obj = institution.classes.classes[klass]
            self.classes.append(class_obj)
            self.nb_students += class_obj.size
        for subject in json['subjects']:
            self.subjects.append(institution.subjects.subjects[subject])
        for duration_idx, duration in enumerate(durations):
            current_idx = self.idx + duration_idx
            if duration == 0.5:
                self.total_duration += 1
                if len(self.teachers) != 1:
                    raise Exception('cannot add half duration for two teachers from now')
                teacher = self.teachers[0]
                half_found = False
                if teacher.half_durations != []:
                    for half_idx, half_duration in enumerate(teacher.half_durations):
                        class_found = False
                        for klass in self.classes:
                            if klass in half_duration.subactivity.classes:
                                class_found = True
                                break
                        if not class_found:
                            half_found = True
                            #found one with different classes than current's activity
                            teacher.half_durations[half_idx].second_activity = self
                            teacher.half_durations[half_idx].nb_students = max(teacher.half_durations[half_idx].nb_students, self.nb_students)
                            teacher.half_durations[half_idx].second_idx = current_idx
                            self.durations.append(teacher.half_durations[half_idx])
                            teacher.half_durations.pop(half_idx)
                            break
                if not half_found:
                    new_half = HalfDuration(current_idx, duration, self)
                    teacher.half_durations.append(new_half)
                    self.durations.append(new_half)
            else:
                self.total_duration += duration
                self.durations.append(Duration(current_idx, duration,
                                               self))
        if len_subactivities != 1 and self.force_nb_students is None:
            raise ParamError('If more than one event in same time, must set force_nb_students')

    def tofet(self, fet, tags_list, activity_list, time_constraintes_same):
        len_durations = len(self.durations)
        if len_durations != 1:
            # if more than on duration for an activity, should not set the same day
            time_constrainte_between = fet.time_constrainte.add('ConstraintMinDaysBetweenActivities')
            time_constrainte_between.add('Weight_Percentage', '95')
            time_constrainte_between.add('Consecutive_If_Same_Day', 'true')
            time_constrainte_between.add('Number_of_Activities', len_durations)
            time_constrainte_between.add('MinDays', '1')
            time_constrainte_between.add('Active', 'true')
            time_constrainte_between.add('Comments')
        else:
            time_constrainte_between = None
        for idx, duration in enumerate(self.durations):
            if time_constraintes_same != []:
                time_constraintes_same[idx].add('Activity_Id', duration.idx)
                add_subgroup = True
            else:
                add_subgroup = False
            duration.tofet(self, tags_list, activity_list, time_constrainte_between, add_subgroup)

    def tojson(self):
        ret = {'teachers': [], 'classes': [], 'subjects': []}
        for teacher in self.teachers:
            ret['teachers'].append(teacher.idx)
        for klass in self.classes:
            ret['classes'].append(klass.idx)
        for subject in self.subjects:
            ret['subjects'].append(subject.idx)
        for duration in self.durations:
            if duration.schedules != []:
                ret.setdefault('schedules', []).extend(duration.schedules)
            #else:
            #    break
        if self.force_nb_students is not None:
            ret['force_nb_students'] = self.force_nb_students
        return ret


class Activity(object):
    def __init__(self, idx, json, institution):
        self.subactivities = []
        self.len_durations = len(json['durations'])
        subactivities = json['subactivities']
        len_subactivities = len(subactivities)
        subidx = idx + 1
        for subactivity in json['subactivities']:
            self.subactivities.append(SubActivity(subidx, subactivity, institution, json['durations'], len_subactivities))
            subidx += self.len_durations

    def tofet(self, tags_list, fet, activity_list):
        len_subactivities = len(self.subactivities)
        time_constraintes_same = []
        if len_subactivities != 1:
            for idx in xrange(self.len_durations):
                # force two subactivity in same period
                time_constrainte_same = fet.time_constrainte.add('ConstraintActivitiesSameStartingTime')
                time_constrainte_same.add('Weight_Percentage', '100')
                time_constrainte_same.add('Number_of_Activities', len_subactivities)
                time_constrainte_same.add('Active', 'true')
                time_constrainte_same.add('Comments')
                time_constraintes_same.append(time_constrainte_same)
        for subactivity in self.subactivities:
            subactivity.tofet(fet, tags_list, activity_list, time_constraintes_same)

    def tojson(self):
        ret = {'subactivities': []}
        for subactivity in self.subactivities:
            ret['subactivities'].append(subactivity.tojson())
        durations = []
        for duration in self.subactivities[0].durations:
            if isinstance(duration, HalfDuration):
                durations.append(0.5)
            else:
                durations.append(duration.duration)
        ret['durations'] = durations
        return ret


class Activities(object):
    def __init__(self, json, institution):
        self.activities = []
        idx = 0
        for activity in json:
            self.activities.append(Activity(idx, activity, institution))
            idx += len(activity['durations'])

    def tofet(self, fet):
        #activity
        xml = fet.add('Activity_Tags_List')
        xml.add('Activity_Tag').add('Name', 'all_rooms')
        activity_list = fet.add('Activities_List')
        for activity in self.activities:
            activity.tofet(xml, fet, activity_list)

    def tojson(self):
        ret = []
        for activity in self.activities:
            ret.append(activity.tojson())
        return ret

    def find(self, idx):
        for activity in self.activities:
            for subactivity in activity.subactivities:
                for duration in subactivity.durations:
                    if duration.idx == idx:
                        return duration
                    if isinstance(duration, HalfDuration) and duration.second_idx == idx:
                        return duration
        raise ParamError('cannot find activity with id {}'.format(idx)) 
            

class TimeTable(object):
    def __init__(self, json):
        self.days = Days(json['institution']['days'], json['max_dominant'])
        self.teachers = Teachers(json['institution']['teachers'])
        self.subjects = Subjects(json['institution']['subjects'])
        self.buildings = Buildings(json['institution']['buildings'])
        self.classes = Classes(json['institution']['classes'])
        self.activities = Activities(json['activities'], self)

    def tofet(self):
        fet = FET()
        self.days.tofet(fet)
        self.classes.tofet(fet)
        self.teachers.tofet(fet)
        self.subjects.tofet(fet)
        #time contraintes
        fet.add_time_constrainte()
        #space constraintes
        fet.add_space_constrainte()
        self.activities.tofet(fet)
        self.buildings.tofet(fet)
        self.days.fetconstraints(fet.time_constrainte, self.activities)
        return fet.tostring()

    def tojson(self):
        dico = {'institution': {'buildings': self.buildings.tojson(),
                                'teachers': self.teachers.tojson(),
                                'classes': self.classes.tojson(),
                                'subjects': self.subjects.tojson(),
                                'days': self.days.tojson()}}
        dico['max_dominant'] = self.days.max_dominant
        dico['activities'] = self.activities.tojson()
        return loads(dumps(dico))

    def fromfet(self, xml):
        for teacher in xml.findall('Teacher'):
            for day in teacher.findall('Day'):
                for hour in day.findall('Hour'):
                    for activity in hour.findall('Activity_Tag'):
                        name = activity.get('name')
                        if name not in ['all_rooms']:
                            duration = self.activities.find(int(name))
                            if isinstance(duration, HalfDuration) and name == str(duration.second_idx):
                                continue
                            schedule = {'day': int(day.get('name')), 'hour': int(hour.get('name').split(':')[0])}
                            room = hour.findall('Room')
                            if len(room) != 1:
                                raise Exception('cannot load more than one room')
                            room = room[0]
                            room_obj = self.buildings.find(room.get('name'))
                            schedule['building'] = room_obj.building.idx
                            schedule['room'] = room_obj.idx
                            duration.schedules.append(schedule)
