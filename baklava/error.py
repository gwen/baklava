# coding: utf8
"""
user based exceptions
"""


class ParamError(Exception):
    pass


class OutOfRangeError(Exception):
    """the expected wish is out of range
    """
    pass
