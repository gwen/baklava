# coding: utf8
"""

time table utilities

"""
from error import OutOfRangeError


def wish2percentage(wish):
    """
        converts a wish to a valid percentage

        :param wish: a integer in [1,2,3,4,5]
        :returns: a percentage, that is a number in range(1,101)
    """
    if wish not in [1,2,3,4,5]:
        raise OutOfRangeError("Not a valid wish: {0}".format(wish))
    w2percent = {1:10, 2:25, 3:50, 4:75, 5:100}
    return w2percent(wish)
