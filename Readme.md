# Baklava

A fantastically configurable automatic scheduler for a school's timetable.

## Table of Contents
1. [Team](#team)
1. [Installation](#installation)
1. [Setup](#setup)
1. [Usage](#usage)
1. [Development](#development)
1. [Contributing](#contributing)

## Team

  - __Lead Developer__: [Gwenaël Rémond](https://gitlab.com/gwen)
  - __Developer__: [Emmanuel Garette](https://gitlab.com/gnunux)

## Installation

- `npm install`
- `npm start`

**Requirements**

On the server side :

- `python`
- `python-flask`
- `python-jsonschema`
