#!/usr/bin/env python
# coding: utf-8

from flask import Flask
from json import dumps

from baklava.config import default_periods

# from flask import request, json
#from jsonschema import validate
#from jsonschema.exceptions import ValidationError

#app = Flask(__name__, static_url_path='')
app = Flask(__name__, static_folder='.')
#app.config.from_object('config')

@app.route('/', methods=['GET'])
def index():
    return app.send_static_file('index.html')

@app.route('/periods', methods=['GET'])
def periods():
    """
    :returns: a list of periods like this::

        [{'day': 0, 'period': 0, 'hour': 0},
                {'day': 0, 'period': 0, 'hour': 1,},
                {'day': 0, 'period': 0, 'hour': 2,},
                {'day': 1, 'period': 0, 'hour': 1,},
                ...
                ]
    """

    return dumps(default_periods)

# FIXME : DEBUG MODE
app.run(host="0.0.0.0", debug=True, port=8080)
