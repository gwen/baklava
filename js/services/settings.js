angular.module('Baklava').service('settings', function(){

    // FIXME put this in a config.js
    default_periods = [

                {'day': 0, 'period': 0, 'hour': 0},
                {'day': 0, 'period': 0, 'hour': 1,},
                {'day': 0, 'period': 0, 'hour': 2,},
                {'day': 0, 'period': 0, 'hour': 3,},
                {'day': 0, 'period': 0, 'hour': 4,},

                {'day': 0, 'period': 1, 'hour': 0},
                {'day': 0, 'period': 2, 'hour': 1,},
                {'day': 0, 'period': 3, 'hour': 2,},
                {'day': 0, 'period': 4, 'hour': 3,},
                {'day': 0, 'period': 5, 'hour': 4,},

                {'day': 1, 'period': 0, 'hour': 0},
                {'day': 1, 'period': 0, 'hour': 1,},
                {'day': 1, 'period': 0, 'hour': 2,},
                {'day': 1, 'period': 0, 'hour': 3,},
                {'day': 1, 'period': 0, 'hour': 4,},

                {'day': 1, 'period': 1, 'hour': 0},
                {'day': 1, 'period': 2, 'hour': 1,},
                {'day': 1, 'period': 3, 'hour': 2,},
                {'day': 1, 'period': 4, 'hour': 3,},
                {'day': 1, 'period': 5, 'hour': 4,},

                {'day': 2, 'period': 0, 'hour': 0},
                {'day': 2, 'period': 0, 'hour': 1,},
                {'day': 2, 'period': 0, 'hour': 2,},
                {'day': 2, 'period': 0, 'hour': 3,},
                {'day': 2, 'period': 0, 'hour': 4,},

                {'day': 2, 'period': 1, 'hour': 0},
                {'day': 2, 'period': 2, 'hour': 1,},
                {'day': 2, 'period': 3, 'hour': 2,},
                {'day': 2, 'period': 4, 'hour': 3,},
                {'day': 2, 'period': 5, 'hour': 4,},

                {'day': 3, 'period': 0, 'hour': 0},
                {'day': 3, 'period': 0, 'hour': 1,},
                {'day': 3, 'period': 0, 'hour': 2,},
                {'day': 3, 'period': 0, 'hour': 3,},
                {'day': 3, 'period': 0, 'hour': 4,},

                {'day': 3, 'period': 1, 'hour': 0},
                {'day': 3, 'period': 2, 'hour': 1,},
                {'day': 3, 'period': 3, 'hour': 2,},
                {'day': 3, 'period': 4, 'hour': 3,},
                {'day': 3, 'period': 5, 'hour': 4,},

                {'day': 4, 'period': 0, 'hour': 0},
                {'day': 4, 'period': 0, 'hour': 1,},
                {'day': 4, 'period': 0, 'hour': 2,},
                {'day': 4, 'period': 0, 'hour': 3,},
                {'day': 4, 'period': 0, 'hour': 4,},

                {'day': 4, 'period': 1, 'hour': 0},
                {'day': 4, 'period': 2, 'hour': 1,},
                {'day': 4, 'period': 3, 'hour': 2,},
                {'day': 4, 'period': 4, 'hour': 3,},
                {'day': 4, 'period': 5, 'hour': 4,},

                {'day': 5, 'period': 0, 'hour': 0},
                {'day': 5, 'period': 0, 'hour': 1,},
                {'day': 5, 'period': 0, 'hour': 2,},
                {'day': 5, 'period': 0, 'hour': 3,},
                {'day': 5, 'period': 0, 'hour': 4,},

                {'day': 5, 'period': 1, 'hour': 0},
                {'day': 5, 'period': 2, 'hour': 1,},
                {'day': 5, 'period': 3, 'hour': 2,},
                {'day': 5, 'period': 4, 'hour': 3,},
                {'day': 5, 'period': 5, 'hour': 4,}
                ]


});
