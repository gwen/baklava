angular.module('Baklava').controller('MainCtrl', ['$scope', function($scope){
    $scope.morningData = [
      {
        "lundi": "première",
        "mardi": "6èm1",
        "mercredi": "6ème3",
        "jeudi": "3ème5",
        "vendredi": "4èmeC"
       },
       {
         "lundi": "seconde",
         "mardi": "6èm1",
         "mercredi": "6ème3",
         "jeudi": "3ème5",
         "vendredi": "4èmeC"
        },
        {
          "lundi": "troisième",
          "mardi": "6èm1",
          "mercredi": "6ème3",
          "jeudi": "3ème5",
          "vendredi": "4èmeC"
         }
    ];
    $scope.getIndex = function(row){
	     return $scope.morningRows.indexOf(row);
    };
    $scope.getMorningData = function(colIndex){
	     return $scope.morningData[colIndex].mardi;
    };

    // $scope.afternoonData = [
    //   [true, false, true, false, false],
    //   [true, false, true, true, true],
    //   [true, false, true, true, false]
    // ];
    $scope.columns = ["lundi", "mardi", "mercredi", "jeudi", "vendredi"];
    $scope.morningRows = ["8H00~9H00", "9H00~10H00", "11H00~12H00"];
    // $scope.morningRowSize = morningRows.length;
    $scope.afternoonRows = ["13H30~14H30", "14H30~15H30", "16H00~17H00"];
    // $scope.afternoonRowSize = afternoonRows.length;

}]);
