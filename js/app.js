var Baklava = angular.module('Baklava', ['pascalprecht.translate']);

//var Baklava = angular.module('Baklava', ['pascalprecht.translate', 'ui.bootstrap', 'ngTouch', 'ui.grid']);
var enTranslations = require('../i18n/en.js').translations;
var frTranslations = require('../i18n/fr.js').translations;

// i18n
Baklava.config(['$translateProvider', function ($translateProvider) {
  $translateProvider.useSanitizeValueStrategy('escape');
  $translateProvider.translations('en', enTranslations);
  $translateProvider.translations('fr', frTranslations);
  $translateProvider.preferredLanguage('fr');
}]);

Baklava.constant('Version', 'v1.00');

require('./services');
require('./controllers');
