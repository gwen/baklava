exports.translations = {
    'APPSHORTTITLE': 'Baklava',
    'APPTITLE':'Baklava timetable Editor',
    'CREDITS': {
        'ANNONCE': 'Baklava timetable editor',
        'LICENCE': 'AGPL licence 2016-2017.'
    }
};
