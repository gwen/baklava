exports.translations = {
    'APPSHORTTITLE': 'Baklava',
    'APPTITLE':'Baklava éditeur d\'emploi du temps',
    'CREDITS': {
        'ANNONCE': 'Baklava éditeur d\'emploi du temps',
        'LICENCE': 'licence AGPL 2016-2017.'
    }
};
